﻿using System;

namespace OOP
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Pokemon
            static void MakePokemon()
            {
                Pokemon[] pokemons = new Pokemon[4];
                for (int i = 0; i < pokemons.Length; i++)
                {
                    pokemons[i] = new Pokemon();
                    pokemons[i].MaxHP = 20;
                    pokemons[i].HP = 20;
                }
                pokemons[0].PokeSpecie = PokeSpecies.Bulbasaur;
                pokemons[1].PokeSpecie = PokeSpecies.Charmander;
                pokemons[2].PokeSpecie = PokeSpecies.Squirtle;
                pokemons[3].PokeSpecie = PokeSpecies.Pikachu;
                pokemons[0].PokeType = PokeTypes.Grass;
                pokemons[1].PokeType = PokeTypes.Fire;
                pokemons[2].PokeType = PokeTypes.Water;
                pokemons[3].PokeType = PokeTypes.Electric;
                for (int i = 0; i < pokemons.Length; i++)
                {
                    pokemons[i].Attack();
                }
            }

            static void MakePokemonWithConstructor()
            {
                Pokemon bulbasaurConstructor = new Pokemon(20, 20, PokeSpecies.Bulbasaur, PokeTypes.Grass);
                Pokemon charmanderConstructor = new Pokemon(20, 20, PokeSpecies.Charmander, PokeTypes.Fire);
                Pokemon pikachuConstructor = new Pokemon(20, 20, PokeSpecies.Pikachu, PokeTypes.Electric);
                Pokemon squirtleConstructor = new Pokemon(20, 20, PokeSpecies.Squirtle, PokeTypes.Water);
                bulbasaurConstructor.Attack();
                charmanderConstructor.Attack();
                pikachuConstructor.Attack();
                squirtleConstructor.Attack();
            }

            static void MakePokemonChained()
            {
                Pokemon squirtle = new Pokemon(20, PokeSpecies.Squirtle, PokeTypes.Water);
                Pokemon charmander = new Pokemon(40, PokeSpecies.Charmander, PokeTypes.Fire);
                Pokemon bulbasaur = new Pokemon(60, PokeSpecies.Bulbasaur, PokeTypes.Grass);
                Pokemon pikachu = new Pokemon(80, PokeSpecies.Pikachu, PokeTypes.Electric);
                Console.WriteLine($"De nieuwe {squirtle.PokeSpecie} heeft maximum {squirtle.MaxHP} HP en heeft momenteel {squirtle.HP} HP.");
                Console.WriteLine($"De nieuwe {charmander.PokeSpecie} heeft maximum {charmander.MaxHP} HP en heeft momenteel {charmander.HP} HP.");
                Console.WriteLine($"De nieuwe {bulbasaur.PokeSpecie} heeft maximum {bulbasaur.MaxHP} HP en heeft momenteel {bulbasaur.HP} HP.");
                Console.WriteLine($"De nieuwe {pikachu.PokeSpecie} heeft maximum {pikachu.MaxHP} HP en heeft momenteel {pikachu.HP} HP.");
            }

            static void TestConsiousPokemonSafe()
            {
                Pokemon[] pokemons = new Pokemon[4];
                for (int i = 0; i < pokemons.Length; i++)
                {
                    pokemons[i] = new Pokemon();
                    pokemons[i].MaxHP = 20;
                }
                pokemons[0].HP = 0;
                pokemons[1].HP = 0;
                pokemons[2].HP = 2;
                pokemons[3].HP = 0;
                pokemons[0].PokeSpecie = PokeSpecies.Bulbasaur;
                pokemons[1].PokeSpecie = PokeSpecies.Charmander;
                pokemons[2].PokeSpecie = PokeSpecies.Squirtle;
                pokemons[3].PokeSpecie = PokeSpecies.Pikachu;
                pokemons[0].PokeType = PokeTypes.Grass;
                pokemons[1].PokeType = PokeTypes.Fire;
                pokemons[2].PokeType = PokeTypes.Water;
                pokemons[3].PokeType = PokeTypes.Electric;

                

                Pokemon firstPokemon = Pokemon.FirstConsiousPokemon(pokemons);
                firstPokemon.Attack();


                Pokemon.DemoRestoreHP(pokemons);
                Pokemon.DemoFight(pokemons[0],pokemons[1]);



            }

            static void DemonstratePokeCounter()
            {
                Pokemon bulbasaur1 = new Pokemon(20, 20, PokeSpecies.Bulbasaur, PokeTypes.Grass);
                Pokemon bulbasaur2 = new Pokemon(20, 20, PokeSpecies.Bulbasaur, PokeTypes.Grass);
                Pokemon charmander = new Pokemon(20, 20, PokeSpecies.Charmander, PokeTypes.Fire);
                Pokemon pikachu = new Pokemon(20, 20, PokeSpecies.Pikachu, PokeTypes.Electric);
                Pokemon squirtle = new Pokemon(20, 20, PokeSpecies.Squirtle, PokeTypes.Water);
                for (int i = 0; i <= 5; i++)
                {
                    bulbasaur1.Attack();
                }
                for (int i = 0; i <= 6; i++)
                {
                    bulbasaur2.Attack();
                }
                for (int i = 0; i <= 7; i++)
                {
                    charmander.Attack();
                  
                }
                for (int i = 0; i <= 8; i++)
                {
                    pikachu.Attack();
                }
                for (int i = 0; i <= 9; i++)
                {
                    squirtle.Attack();
                }



                Console.WriteLine($"Hoeveel keer heeft elk type gevochten? Grass:{Pokemon.Grass}  Fire:{Pokemon.Fire}  Water:{Pokemon.Water}  Electric:{Pokemon.Elek}");
            }

            

            MakePokemon();
            MakePokemonWithConstructor();
            MakePokemonChained();
            TestConsiousPokemonSafe();
            DemonstratePokeCounter();
            #endregion

            Console.ReadLine();
        }

    }
}
