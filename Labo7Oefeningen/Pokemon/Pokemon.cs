﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    public enum PokeSpecies { Bulbasaur, Charmander, Squirtle, Pikachu };
    public enum PokeTypes { Grass, Fire, Water, Electric };

    public enum FightOutcomes { WIN, LOSS, UNDECIDED };
    class Pokemon
    {
        public Pokemon()
        {

        }

        public Pokemon(int maxHp, int hp, PokeSpecies specie, PokeTypes type)
        {
            MaxHP = maxHp;
            hp = HP;
            PokeSpecie = specie;
            PokeType = type;
        }

        public Pokemon(int maxHp, PokeSpecies specie, PokeTypes type) : this(maxHp, maxHp, specie, type)
        {
            HP = maxHp / 2;
        }

        private static int grass = 0;
        public static int Grass { get { return grass; } }

        private static int fire = 0;
        public static int Fire { get { return fire; } }

        private static int water = 0;
        public static int Water { get { return water; } }

        private static int elek = 0;
        public static int Elek { get { return elek; } }

        private int maxHP;

        public int MaxHP
        {
            get { return maxHP; }
            set
            {
                if (value <= 1000)
                {
                    if (value >= 20)
                    {
                        maxHP = value;
                    }
                    else maxHP = 20;

                }
                else maxHP = 1000;
            }
        }
        private int hp;

        public int HP
        {
            get { return hp; }
            set { if (value <= 0) { hp = 0; } else if (value >= MaxHP) { hp = MaxHP; } else { hp = value; } }
        }
        public PokeSpecies PokeSpecie { get; set; }
        public PokeTypes PokeType { get; set; }

        public void Attack()
        {
            switch (PokeType)
            {
                case PokeTypes.Grass:
                    Console.ForegroundColor = ConsoleColor.Green;
                    grass++;
                    break;
                case PokeTypes.Fire:
                    Console.ForegroundColor = ConsoleColor.Red;
                    fire++;
                    break;
                case PokeTypes.Water:
                    Console.ForegroundColor = ConsoleColor.Blue;
                    water++;
                    break;
                case PokeTypes.Electric:
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    elek++;
                    break;
            }
            Console.WriteLine($"{PokeSpecie.ToString().ToUpper()}!");
            Console.ResetColor();

        }

        public static Pokemon FirstConsiousPokemon(Pokemon[] pokemon)
        {
            for (int i = 0; i < pokemon.Length; i++)
            {
                if (pokemon[i].HP > 0)
                {
                    return pokemon[i];
                }

            }
            return null;
        }

        public static void RestoreHP(Pokemon pokemon)
        {
            pokemon.HP = pokemon.MaxHP;
        }

        public static void DemoRestoreHP(Pokemon[] pokemon)
        {
            // aanmaken van array bewusteloze Pokemon van 4 soorten zoals eerder: zelf doen
            for (int i = 0; i < pokemon.Length; i++)
            {
                Pokemon.RestoreHP(pokemon[i]);
                Console.WriteLine(pokemon[i].HP);
            }
        }


        public static void FightOutcome(Pokemon fighter1, Pokemon fighter2, Random randomValue)
        {
            FightOutcomes fightOutcome;
            int startingTurn = randomValue.Next(0, 2);


            if (fighter1 != null && fighter2 != null)
            {
                if (startingTurn == 1)
                {
                    Console.WriteLine("You start");
                    while (fighter1.HP > 0 && fighter2.HP > 0)
                    {
                        int randomDamage = randomValue.Next(0, 21);
                        fighter2.HP -= randomDamage;
                        Console.WriteLine($"You attacked for {randomDamage}! Remaining Enemy HP: {fighter2.HP}");
                        randomDamage = randomValue.Next(0, 21);
                        fighter1.HP -= randomDamage;
                        Console.WriteLine($"The enemy attacked for {randomDamage}! Your Remaining HP: {fighter1.HP}");
                        Console.ReadLine();
                    }
                }
                else
                {
                    Console.WriteLine("Enemy starts");
                    while (fighter1.HP > 0 && fighter2.HP > 0)
                    {
                        int randomDamage = randomValue.Next(0, 21);
                        fighter1.HP -= randomDamage;
                        Console.WriteLine($"The enemy attacked for {randomDamage}! Your Remaining HP: {fighter1.HP}");
                        randomDamage = randomValue.Next(0, 21);
                        fighter2.HP -= randomDamage;
                        Console.WriteLine($"You attacked for {randomDamage}! Remaining Enemy HP: {fighter2.HP}");
                        Console.ReadLine();
                    }

                }
                if (fighter1.HP <= 0)
                {
                    fightOutcome = FightOutcomes.LOSS;
                }
                else fightOutcome = FightOutcomes.WIN;
            }
            else fightOutcome = FightOutcomes.UNDECIDED;
            switch (fightOutcome)
            {
                case FightOutcomes.WIN:
                    Console.WriteLine("You won");
                    break;
                case FightOutcomes.LOSS:
                    Console.WriteLine("You lost");
                    break;
                case FightOutcomes.UNDECIDED:
                    Console.WriteLine("Undecided match");
                    break;
            }
        }

        public static void DemoFight(Pokemon fighter1, Pokemon fighter2)
        {
            Random randomPokemon = new Random();

            FightOutcome(fighter1, fighter2, randomPokemon);
        }
    }
}

