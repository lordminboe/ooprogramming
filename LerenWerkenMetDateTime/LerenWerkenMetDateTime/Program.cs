﻿using System;

namespace LerenWerkenMetDateTime
{
    class Program
    {
        static void Main(string[] args)
        {
            Ticks2000Program ticksProgram = new Ticks2000Program();
            ArrayTimerProgram timer = new ArrayTimerProgram();
            ticksProgram.DisplayTicks();
            timer.TimeArray();
            Console.ReadLine();
            
        }
    }
}
