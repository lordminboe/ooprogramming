﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LerenWerkenMetDateTime
{
    class Ticks2000Program
    {
        public void DisplayTicks()
        {
            DateTime startYear = new DateTime(2000, 1, 1);
            DateTime currentYear = DateTime.Now;
            long ticks = currentYear.Ticks - startYear.Ticks;
            Console.WriteLine($"sinds {startYear.ToString("dd MMMM yyyy")} zijn er {ticks} voorbij gegaan ");
        }
    }
}
