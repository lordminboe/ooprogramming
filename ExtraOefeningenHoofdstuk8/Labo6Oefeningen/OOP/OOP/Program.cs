﻿using System;

namespace OOP
{
    class Program
    {
        public static Random rng = new Random();
        static void Main(string[] args)
        {
            Console.WriteLine("Wat wilt u runnen?");
            int keuze = Convert.ToInt32(Console.ReadLine());
            if (keuze == 1)
            {
                BarkingDog dog1 = new BarkingDog();
                BarkingDog dog2 = new BarkingDog();
                dog1.Name = "Swieber";
                dog2.Name = "Misty";
                dog1.Breed = BarkingDog.RandomBreed(rng.Next(0, 4));
                dog2.Breed = BarkingDog.RandomBreed(rng.Next(0, 4));
                Console.WriteLine(dog1.Breed);
                Console.WriteLine(dog2.Breed);
                while (true)
                {
                    Console.WriteLine(dog1.Bark());
                    Console.WriteLine(dog2.Bark());
                }
            }
            else if (keuze == 2)
            {
                Rapport student1 = new Rapport(68);
                Console.WriteLine(student1.PrintHonors());
            }
            else if (keuze == 3)
            {
                RapportV2 student1 = new RapportV2(50);
                Console.WriteLine(student1.ComputeHonors());
            }
            else if (keuze == 4)
            {
                NumberCombination paar1 = new NumberCombination(12, 34);
                Console.WriteLine("Sum = " + paar1.Som());
                Console.WriteLine("Verschil = " + paar1.Verschill());
                Console.WriteLine("Product = " + paar1.Product());
                Console.WriteLine("Quotient = " + paar1.Quotient());
            }
            else if (keuze == 5)
            {
                Rectangle rect = new Rectangle();
                Rectangle rect2 = new Rectangle();
                Triangle triangle = new Triangle();
                Triangle triangle2 = new Triangle();
                rect.Width = -1;
                Console.WriteLine($"{rect.Surface:F1}");
                rect.Width = 0;
                Console.WriteLine($"{rect.Surface:F1}");
                rect.Width = 2.2;
                rect.Height = 1.5;
                Console.WriteLine($"{rect.Surface:F1}");
                rect2.Width = 3;
                Console.WriteLine($"{rect2.Surface:F1}");
                triangle.Base = 3;
                Console.WriteLine($"{triangle.Surface:F1}");
                triangle2.Base = 2;
                triangle2.Height = 2;
                Console.WriteLine($"{triangle2.Surface:F1}");


            }
            else
            {
                Console.WriteLine("Niet beschikbaar, Bye bye");
            }




            Console.ReadLine();
        }
    }
}
