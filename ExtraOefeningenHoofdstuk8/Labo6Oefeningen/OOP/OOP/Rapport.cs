﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class Rapport
    {
        public Rapport(int punten)
        {
            puntenStudent = punten;
        }

        private int puntenStudent;

        public string PrintHonors()
        {
            if (puntenStudent < 50)
            {
                return "Niet geslaagd";
            }
            else if (puntenStudent >= 50 && puntenStudent < 68)
            {
                return "Voldoende";
            }
            else if (puntenStudent >= 68 && puntenStudent < 75)
            {
                return "Onderscheiding";
            }
            else if (puntenStudent >= 75 && puntenStudent < 85)
            {
                return "Grote onderscheiding";
            }
            else if (puntenStudent > 85)
            {
                return "Grootste onderscheiding";
            }
            else
            {
                return "Er liep iets fout, je hebt geen geldige punten ingegeven";
            }
        }
    }
}
