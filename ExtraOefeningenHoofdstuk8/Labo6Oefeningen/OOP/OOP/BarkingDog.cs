﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class BarkingDog
    {
        public string Name;
        public string Breed;
        public int RandomBreedNumber { get; set; }

        public static string RandomBreed(int RandomBreedNumber)
        {
            if (RandomBreedNumber == 0)
            {
                return "German Shepherd";
            }
            else if (RandomBreedNumber == 1)
            {
                return "Wolfspitz";
            }
            else
            {
                return "Chihuahua";
            }

        }

        public string Bark()
        {
            if (Breed == "German Shepherd")
            {
                return "RUFF!";
            }
            else if (Breed == "Wolfspitz")
            {
                return "AwawaWAF!";
            }
            else if (Breed == "Chihuahua")
            {
                return "ARF ARF ARF!";
            }
            // dit zou nooit mogen gebeuren
            // maar als de programmeur van Main iets fout doet, kan het wel
            else
            {
                return "Euhhh... Miauw?";
            }
        }
    }
}
