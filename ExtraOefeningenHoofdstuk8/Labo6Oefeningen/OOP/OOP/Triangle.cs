﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class Triangle
    {
        public double Base { get; set; } = 1.0;

        public double Height { get; set; } = 1.0;


        public double Surface
        {
            get
            {
                if (Base <= 0)
                {
                    Console.Write($"Je mag een driehoek zijn base niet zetten op ");
                    return Base;

                }
                else if (Height <= 0)
                {
                    Console.Write($"Je mag een driehoek zijn height niet zetten op ");
                    return Height;

                }
                else
                {
                    Console.Write($"Een driehoek met een base van {Base} en een hoogte van {Height} heeft een oppervalkte van ");
                    return (Base * Height) / 2;
                }
            }
        }
    }
}
