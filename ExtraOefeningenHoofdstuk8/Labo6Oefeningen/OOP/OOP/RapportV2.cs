﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    public enum Honors { Niet_Geslaagd, Voldoende, Onderscheiding, Grote_Onderscheiding, Grootste_Onderscheiding };
    class RapportV2
    {
        public RapportV2(int punten)
        {
            puntenStudent = punten;
        }
        private int puntenStudent;

        public string ComputeHonors()
        {
            if (puntenStudent < 50)
            {
                return Honors.Niet_Geslaagd.ToString();
            }
            else if (puntenStudent >= 50 && puntenStudent < 68)
            {
                return Honors.Voldoende.ToString();
            }
            else if (puntenStudent >= 68 && puntenStudent < 75)
            {
                return Honors.Onderscheiding.ToString();
            }
            else if (puntenStudent >= 75 && puntenStudent < 85)
            {
                return Honors.Grote_Onderscheiding.ToString();
            }
            else if (puntenStudent > 85)
            {
                return Honors.Grootste_Onderscheiding.ToString();
            }
            else
            {
                return "Er liep iets fout, je hebt geen geldige punten ingegeven";
            }
        }
    }
}
