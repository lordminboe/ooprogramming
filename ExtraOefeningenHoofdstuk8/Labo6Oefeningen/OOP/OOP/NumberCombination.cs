﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class NumberCombination
    {
        public NumberCombination(int _nummer1, int _nummer2)
        {
            nummer1 = _nummer1;
            nummer2 = _nummer2;
        }
        private int nummer1;
        private int nummer2;

        public double Som()
        {
            return nummer1 + nummer2;
        }
        public double Verschill()
        {
            return nummer1 - nummer2;
        }

        public double Product()
        {
            return nummer1 * nummer2;
        }

        public double Quotient()
        {
            if (nummer1 != 0 || nummer2 != 0)
            {
                return Convert.ToDouble(nummer1) / Convert.ToDouble(nummer2);
            }
            else
            {
                Console.WriteLine("Error");
                return 0;
            }

        }


    }
}
